package com.company.service;

import com.company.client.UserManagementClient;
import com.company.constants.Category;
import com.company.domain.Book;
import com.company.exception.DuplicateResourceException;
import com.company.exception.ResourceNotFoundException;
import com.company.model.dto.BookCreateDto;
import com.company.model.dto.BookResponseDto;
import com.company.model.dto.BookUpdateDto;
import com.company.repository.BookRepository;
import com.company.security.auth.service.SecurityService;
import com.company.service.mapper.BookMapper;
import com.company.service.search.SearchFilter;
import com.company.service.search.SearchSpecification;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class BookStoreServiceImpl implements BookStoreService {

    private final BookRepository bookRepository;

    private final BookMapper bookMapper;
    private final SecurityService securityService;
    private final UserManagementClient userManagementClient;

    @Override
    @Transactional
    public void create(BookCreateDto dto) {
        boolean check = bookRepository.existsByTitleAndCategory(dto.getTitle(), Category.valueOf(dto.getCategory()));
        if (check) throw new DuplicateResourceException("Book already exists:");

        Book book = Book.builder()
                .title(dto.getTitle())
                .category(Category.valueOf(dto.getCategory()))
                .price(dto.getPrice())
                .totalCount(dto.getTotalCount())
                .uuid(UUID.randomUUID().toString())
                .userUuid(getCurrentUserUuid())
                .build();

        bookRepository.save(book);
    }

    @Override
    public void addBook(String uuid, int quantityToAdd) {
        Book book = bookRepository.findByUuid(uuid)
                .orElseThrow(() -> new ResourceNotFoundException("Book with id:" + uuid + " is not registered. Use addNewBook to register."));

        int totalCountAfterAdd = book.getTotalCount() + quantityToAdd;
        book.setTotalCount(totalCountAfterAdd);

        bookRepository.save(book);
    }

    @Override
    public BookResponseDto getBookById(String uuid) {
        Book book = bookRepository.findByUuid(uuid)
                .orElseThrow(() -> new ResourceNotFoundException("Book with id:" + uuid + " is not found."));

        return bookMapper.mapToDto(book);
    }


    @Override
    public List<BookResponseDto> getAllBooks() {
        List<Book> books = bookRepository.findAll();
        return this.mapToDtoList(books);
    }

    @Override
    public int getNumberOfBooksById(String uuid) {
        Optional<Book> book = bookRepository.findByUuid(uuid);

        //If book is present get Total Count else return 0
        return book.map(Book::getTotalCount).orElse(0);
    }

    @Override
    public void updateBook(String uuid, BookUpdateDto bookUpdateDto) {
        Book book = bookRepository.findByUuid(uuid)
                .orElseThrow(() -> new ResourceNotFoundException("Book with id:" + uuid + " is not registered."));

        book.setUserUuid(getCurrentUserUuid());
        book.setTitle(bookUpdateDto.getTitle());
        book.setCategory(bookUpdateDto.getCategory());
        book.setPrice(bookUpdateDto.getPrice());
        book.setTotalCount(bookUpdateDto.getTotalCount());

        bookRepository.save(book);
    }

    @Override
    public List<BookResponseDto> search(SearchFilter filter) {
        SearchSpecification searchSpecification = new SearchSpecification();
        searchSpecification.search(filter);
        List<Book> books = bookRepository.findAll(searchSpecification);

        return this.mapToDtoList(books);
    }

    private List<BookResponseDto> mapToDtoList(List<Book> books) {
        return books.stream().map(book -> {
            return BookResponseDto.builder()
                    .id(book.getId())
                    .uuid(book.getUuid())
                    .title(book.getTitle())
                    .category(book.getCategory())
                    .price(book.getPrice())
                    .totalCount(book.getTotalCount())
                    .createdAt(book.getCreatedAt())
                    .userResponseDto(userManagementClient.getByUuid(book.getUserUuid()).getBody())
                    .build();
        }).collect(Collectors.toList());
    }

    private String getCurrentUserUuid() {
        return securityService.getCurrentUserUuid().orElseThrow(
                () -> new ResourceNotFoundException("Current user uuid not found"));
    }

}
