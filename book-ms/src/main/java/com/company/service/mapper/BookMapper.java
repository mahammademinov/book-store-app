package com.company.service.mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

import com.company.domain.Book;
import com.company.model.dto.BookCreateDto;
import com.company.model.dto.BookResponseDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface BookMapper {

//    @Mapping(source = "title", target = "title")
//    @Mapping(source = "category", target = "category")
//    @Mapping(source = "price", target = "price")
//    @Mapping(source = "totalCount", target = "totalCount")
    Book mapToEntity(BookCreateDto bookCreateDto);

    BookResponseDto mapToDto(Book book);

}
