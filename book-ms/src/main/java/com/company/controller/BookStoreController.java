package com.company.controller;

import com.company.model.dto.BookCreateDto;
import com.company.model.dto.BookResponseDto;
import com.company.model.dto.BookUpdateDto;
import com.company.service.BookStoreService;
import com.company.service.search.SearchMappingFilter;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/books")
@RequiredArgsConstructor
public class BookStoreController {

    private final BookStoreService bookStoreService;
    private final SearchMappingFilter filter;

    @PostMapping("/add-new-book")
    @ResponseStatus(HttpStatus.CREATED)
    public void addNewBook(@Valid @RequestBody BookCreateDto bookDto) {
        bookStoreService.create(bookDto);
    }

    @PutMapping("/add-book/{uuid}/{quantityToAdd}")
    @ResponseStatus(HttpStatus.OK)
    public void addBook(@PathVariable String uuid,
                        @PathVariable int quantityToAdd) {
        bookStoreService.addBook(uuid, quantityToAdd);
    }

    @GetMapping("/search")
    public ResponseEntity<List<BookResponseDto>> search(
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @RequestParam(required = false, defaultValue = "10") Integer size,
            @RequestParam(required = false) String title,
            @RequestParam(required = false) String author,
            @RequestParam(required = false) String category,
            @RequestParam(required = false) Float price,
            @RequestParam(required = false) Integer totalCount) {
        return ResponseEntity.ok(bookStoreService.search(
                filter.setMapFilter(page, size, title, author, category, price, totalCount)
        ));
    }

    @GetMapping("/{uuid}")
    public BookResponseDto getBookById(@PathVariable String uuid) {
        return bookStoreService.getBookById(uuid);
    }

    @GetMapping("/book-list")
    public List<BookResponseDto> getAllBooks() {
        return bookStoreService.getAllBooks();
    }

    @GetMapping("/number-of-books/{uuid}")
    public int getNumberOfBooksById(@PathVariable String uuid) {
        return bookStoreService.getNumberOfBooksById(uuid);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public void updateBook(@RequestParam String uuid,
                           @Valid @RequestBody BookUpdateDto bookUpdateDto) {
        bookStoreService.updateBook(uuid, bookUpdateDto);
    }

}
