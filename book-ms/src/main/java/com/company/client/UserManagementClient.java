package com.company.client;

import com.company.dto.RegisterDto;
import com.company.dto.UserResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(
        name = "usermanagement",
        url = "${ms.usermanagement.url}"
)
public interface UserManagementClient {

    @RequestMapping("/{uuid}")
    ResponseEntity<UserResponseDto> getByUuid(@PathVariable("uuid") String uuid);

    @PostMapping("/register")
    void register(@RequestBody RegisterDto reqDto);

}
