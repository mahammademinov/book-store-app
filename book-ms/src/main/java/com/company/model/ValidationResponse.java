package com.company.model;

import java.util.List;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidationResponse extends ApiResponse {
    private List<String> errors;

    public ValidationResponse(List<String> errors) {
        super(false, "Validation error");
        this.errors = errors;
    }
}
