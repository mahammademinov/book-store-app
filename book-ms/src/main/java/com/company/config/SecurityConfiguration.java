package com.company.config;

import static com.company.constant.UserAuthority.PUBLISHER;
import static com.company.constant.UserAuthority.USER;
import static com.company.constant.http.HttpConstants.SUB_PATH;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

import com.company.security.auth.AuthenticationEntryPointConfigurer;
import com.company.security.auth.service.AuthService;
import com.company.security.auth.service.JwtService;
import com.company.security.config.BaseSecurityConfig;
import com.company.security.config.SecurityProperties;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Slf4j
@Import({
        SecurityProperties.class, JwtService.class,
        AuthenticationEntryPointConfigurer.class
})
@EnableWebSecurity
public class SecurityConfiguration extends BaseSecurityConfig {
    private static final String BOOK_API = "/api/books";

    public SecurityConfiguration(SecurityProperties securityProperties, List<AuthService> authServices) {
        super(securityProperties, authServices);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(GET, BOOK_API + SUB_PATH).permitAll();
//                .antMatchers(POST, BOOK_API+SUB_PATH).hasAuthority(PUBLISHER.name())
//                .antMatchers(BOOK_API + SUB_PATH).access(authorities(USER));

        super.configure(http);
    }
}
