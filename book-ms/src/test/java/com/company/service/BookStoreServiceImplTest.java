package com.company.service;


import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import com.company.constants.Category;
import com.company.domain.Book;
import com.company.dto.UserResponseDto;
import com.company.exception.ControllerException;
import com.company.exception.DuplicateResourceException;
import com.company.exception.ResourceNotFoundException;
import com.company.model.dto.BookCreateDto;
import com.company.model.dto.BookResponseDto;
import com.company.repository.BookRepository;
import com.company.security.auth.service.SecurityService;
import com.company.service.mapper.BookMapper;
import java.time.Instant;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class BookStoreServiceImplTest {

    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_STRING = "string";
    private static final Float DUMMY_FLOAT = 55F;
    private static final Integer DUMMY_INTEGER = 5;
    private static final Instant DUMMY_INSTANT = Instant.now();


    @InjectMocks
    private BookStoreServiceImpl bookStoreService;

    Book book;
    BookCreateDto bookCreateDto;
    BookResponseDto bookResponseDto;
    UserResponseDto userResponseDto;

    @Mock
    BookRepository bookRepository;

    @Mock
    BookMapper bookMapper;

    @Mock
    SecurityService securityService;

    @BeforeEach
    void setUp() {
        book = Book.builder()
                .id(DUMMY_ID)
                .uuid(DUMMY_STRING)
                .title(DUMMY_STRING)
                .category(Category.ACTION)
                .price(DUMMY_FLOAT)
                .totalCount(DUMMY_INTEGER)
                .userUuid(DUMMY_STRING)
                .build();

        bookCreateDto = BookCreateDto.builder()
                .title(DUMMY_STRING)
                .category(Category.ACTION.name())
                .price(DUMMY_FLOAT)
                .totalCount(DUMMY_INTEGER)
                .build();

        userResponseDto = UserResponseDto.builder()
                .id(DUMMY_STRING)
                .name(DUMMY_STRING)
                .surname(DUMMY_STRING)
                .email(DUMMY_STRING)
                .build();

        bookResponseDto = BookResponseDto.builder()
                .id(DUMMY_ID)
                .uuid(DUMMY_STRING)
                .title(DUMMY_STRING)
                .category(Category.ACTION)
                .price(DUMMY_FLOAT)
                .totalCount(DUMMY_INTEGER)
                .createdAt(DUMMY_INSTANT)
                .userResponseDto(userResponseDto)
                .build();
    }

    @Test
    void givenBookCreateDto_WhenSaveBook_ThenSuccess() {
        when(bookRepository.existsByTitleAndCategory(any(), any())).thenReturn(false);
        when(securityService.getCurrentUserUuid()).thenReturn(Optional.of(DUMMY_STRING));

        bookStoreService.create(bookCreateDto);

        verify(bookRepository, times(1)).existsByTitleAndCategory(any(), any());
        verify(bookRepository, times(1)).save(any());

        ArgumentCaptor<Book> captor = ArgumentCaptor.forClass(Book.class);
        verify(bookRepository, times(1)).save(captor.capture());

        Book book = captor.getValue();
        assertEquals(bookCreateDto.getTitle(), book.getTitle());
        assertEquals(bookCreateDto.getPrice(), book.getPrice());
        assertEquals(bookCreateDto.getTotalCount(), book.getTotalCount());
        assertEquals(bookCreateDto.getCategory(), book.getCategory().name());
        assertEquals(DUMMY_STRING, book.getUserUuid());

    }

    @Test
    void givenBookCreateDto_WhenSaveBook_ThenDuplicateResourceException() {
        when(bookRepository.existsByTitleAndCategory(bookCreateDto.getTitle(),
                Category.valueOf(bookCreateDto.getCategory()))).thenReturn(true);

        assertThatThrownBy(() -> bookStoreService.create(bookCreateDto))
                .isInstanceOf(DuplicateResourceException.class)
                .hasMessage("Book already exists:");
    }

    @Test
    void givenUuidAndQuantityToAdd_WhenAddBook_ThenSuccess() {
        when(bookRepository.findByUuid(anyString())).thenReturn(Optional.of(book));

        bookStoreService.addBook(DUMMY_STRING, DUMMY_INTEGER);

        ArgumentCaptor<Book> captor = ArgumentCaptor.forClass(Book.class);
        verify(bookRepository, times(1)).save(captor.capture());

        assertThat(book).isNotNull();

        Integer expectedTotalCount = DUMMY_INTEGER + DUMMY_INTEGER;

        Book capture = captor.getValue();
        assertEquals(expectedTotalCount, capture.getTotalCount());
        assertEquals(capture.getUuid(), DUMMY_STRING);
    }

    @Test
    void givenUuidAndQuantityToAdd_WhenAddBook_ThenNotFoundException() {
        when(bookRepository.findByUuid(anyString())).thenReturn(Optional.empty());

        assertThatThrownBy(() -> bookStoreService.addBook(DUMMY_STRING, DUMMY_INTEGER))
                .isInstanceOf(ResourceNotFoundException.class)
                .hasMessage(
                        String.format("Book with id:%s is not registered. Use addNewBook to register.", DUMMY_STRING));
    }

    @Test
    void givenUuid_WhenGetBook_ThenReturnBookResponseDto(){
        when(bookRepository.findByUuid(anyString())).thenReturn(Optional.of(book));
        when(bookMapper.mapToDto(book)).thenReturn(bookResponseDto);

        BookResponseDto dto = bookStoreService.getBookById(DUMMY_STRING);

        verify(bookRepository, times(1)).findByUuid(DUMMY_STRING);
        verify(bookMapper, times(1)).mapToDto(book);

        assertThat(dto).isNotNull();
        assertEquals(book.getUuid(), dto.getUuid());
        assertEquals(book.getUserUuid(), dto.getUserResponseDto().getId());
        assertEquals(book.getTotalCount(), dto.getTotalCount());
    }
}