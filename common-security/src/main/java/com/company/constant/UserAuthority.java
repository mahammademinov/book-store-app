package com.company.constant;

public enum UserAuthority {
    ADMIN, USER, PUBLISHER
}
