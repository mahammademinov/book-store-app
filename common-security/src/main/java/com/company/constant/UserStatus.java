package com.company.constant;

import lombok.Getter;

@Getter
public enum UserStatus {
    INACTIVE(true),
    ACTIVE(true),
    DELETED(false),
    BLOCKED(false),
    TEMPORARY_BLOCKED(true);

    private final boolean enabled;

    UserStatus(boolean enabled) {
        this.enabled = enabled;
    }

}

