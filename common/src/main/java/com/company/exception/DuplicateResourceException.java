package com.company.exception;

import org.springframework.http.HttpStatus;

public class DuplicateResourceException extends ControllerException{

    public DuplicateResourceException(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }
}
