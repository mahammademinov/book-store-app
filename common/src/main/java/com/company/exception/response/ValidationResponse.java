package com.company.exception.response;

import java.util.List;
import lombok.Data;

@Data
public class ValidationResponse extends ApiResponse {
    private List<String> errors;

    public ValidationResponse(List<String> errors) {
        super(false, "Validation error");
        this.errors = errors;
    }
}
