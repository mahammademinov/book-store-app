package com.company.service.mapper;

import com.company.dto.VerificationTokenDto;
import com.company.entity.VerificationToken;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface VerificationTokenMapper {

    VerificationTokenDto toDto(VerificationToken verificationToken);

}
