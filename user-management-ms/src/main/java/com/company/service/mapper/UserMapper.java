package com.company.service.mapper;

import com.company.common.dto.RegisterRequestDto;
import com.company.dto.UserInfoDto;
import com.company.dto.UserResponseDto;
import com.company.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {

    User registerDtoToEntity(RegisterRequestDto registerRequestDto);

    UserResponseDto toUserRespDto(User user);

    UserInfoDto toInfoDto(User user);

}
