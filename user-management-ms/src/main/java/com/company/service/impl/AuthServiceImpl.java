package com.company.service.impl;

import static com.company.constant.UserAuthority.PUBLISHER;
import static com.company.constant.UserAuthority.USER;
import static com.company.constant.UserStatus.ACTIVE;

import com.company.common.dto.JwtTokenDto;
import com.company.common.dto.LoginRequestDto;
import com.company.common.dto.RegisterRequestDto;
import com.company.common.security.SecurityUtil;
import com.company.constant.UserAuthority;
import com.company.dto.UserInfoDto;
import com.company.entity.Authority;
import com.company.entity.User;
import com.company.entity.VerificationToken;
import com.company.exception.AuthorityNotFoundException;
import com.company.exception.ConflictException;
import com.company.repository.AuthorityRepository;
import com.company.repository.UserRepository;
import com.company.repository.VerificationTokenRepository;
import com.company.service.AuthService;
import com.company.service.KafkaMsgPublisher;
import com.company.service.mapper.UserMapper;
import com.company.service.mapper.VerificationTokenMapper;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private final VerificationTokenRepository verificationTokenRepository;
    private final UserMapper userMapper;
    private final VerificationTokenMapper verificationTokenMapper;
    private final PasswordEncoder passwordEncoder;
    private final SecurityUtil securityUtil;

    private final KafkaMsgPublisher kafkaMsgPublisher;

//    @Value("${spring.kafka.topics.reset-password}")
    private String resetPassword="user-management-reset-password-topic";

    @Override
    public void register(RegisterRequestDto registerRequestDto) {
        Optional<User> userOptional = userRepository.findByEmail(registerRequestDto.getEmail());
        if (userOptional.isPresent()) {
            throw new ConflictException("Email already exist");
        }
        User user = userMapper.registerDtoToEntity(registerRequestDto);

        // add authorities to user
        Set<Authority> authorities = new HashSet<>();
        authorities.add(this.findByAuthority(USER));
        if (registerRequestDto.isPublisher()) {
            authorities.add(this.findByAuthority(PUBLISHER));
        }
        user.setAuthorities(authorities);

        user.setStatus(ACTIVE);

        user.setPassword(passwordEncoder.encode(registerRequestDto.getPassword()));

        userRepository.save(user);

    }

    @Override
    public JwtTokenDto login(LoginRequestDto loginRequestDto) {
        String jwt = securityUtil.createAuthentication(loginRequestDto);

        return JwtTokenDto.builder().authToken(jwt).build();
    }

    @Override
    public void forgotPassword(String email) {
        userRepository.findByEmail(email).ifPresent(user -> {
            String token = UUID.randomUUID().toString();
            VerificationToken verificationToken = createPasswordResetTokenForUser(user, token);
            UserInfoDto userInfoDto = userMapper.toInfoDto(user);
            userInfoDto.setVerificationToken(verificationTokenMapper.toDto(verificationToken));
            kafkaMsgPublisher.publish(user.getId(), userMapper.toInfoDto(user), resetPassword);
        });
    }

    //private util methods

    private Authority findByAuthority(UserAuthority authority) {
        return authorityRepository.findByAuthority(authority)
                .orElseThrow(() -> new AuthorityNotFoundException("Authority not found"));
    }

    private VerificationToken createPasswordResetTokenForUser(User user, String token) {
        VerificationToken myToken = VerificationToken.builder()
                .token(token)
                .expiryDate(LocalDateTime.now().plusHours(24))
                .build();
        myToken.setUser(user);
        return verificationTokenRepository.save(myToken);
    }
}
