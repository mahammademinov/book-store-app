package com.company.service.impl;

import com.company.common.dto.UserUpdateDto;
import com.company.dto.ActivateAccountDto;
import com.company.dto.UserResponseDto;
import com.company.entity.User;
import com.company.entity.VerificationToken;
import com.company.exception.TokenNotFoundException;
import com.company.exception.UserNotFoundException;
import com.company.repository.UserRepository;
import com.company.repository.VerificationTokenRepository;
import com.company.service.KafkaMsgPublisher;
import com.company.service.UserService;
import com.company.service.mapper.UserMapper;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final VerificationTokenRepository verificationTokenRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserResponseDto findByUserUuid(String uuid) {
        return userMapper.toUserRespDto(this.findUserByUuid(uuid));

    }

    @Override
    public void update(String userUuid, UserUpdateDto dto) {
        User user = this.findUserByUuid(userUuid);
        user.setName(dto.getName());
        user.setSurname(dto.getSurname());

        userRepository.save(user);
    }

    @Override
    @Transactional
    public void delete(String uuid) {
        userRepository.delete(this.findUserByUuid(uuid));
    }

    @Override
    public void activateAccount(ActivateAccountDto dto) {
        VerificationToken verificationToken = verificationTokenRepository.findByToken(dto.getToken())
                .orElseThrow(TokenNotFoundException::new);
        User user = verificationToken.getUser();
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user.setAccountNonExpired(true);
        user.setCredentialsNonExpired(true);
        user.setEnabled(true);
        user.setAccountNonLocked(true);
        userRepository.save(user);
    }

    private User findUserByUuid(String uuid) {
        return userRepository.findByUuid(uuid).orElseThrow(UserNotFoundException::new);
    }
}
