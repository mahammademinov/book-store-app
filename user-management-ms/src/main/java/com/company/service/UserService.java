package com.company.service;

import com.company.common.dto.UserUpdateDto;
import com.company.dto.ActivateAccountDto;
import com.company.dto.UserResponseDto;

public interface UserService {

    UserResponseDto findByUserUuid(String uuid);

    void update(String customerUuid, UserUpdateDto dto);

    void delete(String uuid);

    void activateAccount(ActivateAccountDto dto);

}
