package com.company.repository;

import com.company.constant.UserAuthority;
import com.company.entity.Authority;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {

    Optional<Authority> findByAuthority(UserAuthority authority);
}
