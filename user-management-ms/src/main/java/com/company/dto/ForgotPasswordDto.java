package com.company.dto;

import lombok.Data;

@Data
public class ForgotPasswordDto {

    private String email;
}
