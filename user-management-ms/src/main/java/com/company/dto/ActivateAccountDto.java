package com.company.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude = "password")
public class ActivateAccountDto {

    @NotNull
    @Size(min = 6, max = 255)
    private String password;

    private String token;

}
