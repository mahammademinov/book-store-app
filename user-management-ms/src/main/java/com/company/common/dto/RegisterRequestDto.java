package com.company.common.dto;

import static com.company.constant.AuthConstants.PASSWORD_MAX_LENGTH;
import static com.company.constant.AuthConstants.PASSWORD_MIN_LENGTH;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString(exclude = "password")
public class RegisterRequestDto {

    @NotBlank
    private String name;

    @NotBlank
    private String surname;

    @Email
    @NotBlank
    private String email;

    @Size(min = PASSWORD_MIN_LENGTH , max = PASSWORD_MAX_LENGTH)
    private String password;

    private boolean publisher;

}
