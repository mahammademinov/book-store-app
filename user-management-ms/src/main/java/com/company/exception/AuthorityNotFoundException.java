package com.company.exception;

public class AuthorityNotFoundException extends NotFoundException{

    public AuthorityNotFoundException(String message) {
        super(message);
    }
}
