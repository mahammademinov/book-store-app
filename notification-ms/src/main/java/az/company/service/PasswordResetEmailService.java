package az.company.service;

import az.company.dto.UserInfoDto;
import freemarker.template.TemplateException;
import java.io.IOException;
import javax.mail.MessagingException;

public interface PasswordResetEmailService {
    void sendEmail(UserInfoDto dto) throws IOException, TemplateException, MessagingException;
}
