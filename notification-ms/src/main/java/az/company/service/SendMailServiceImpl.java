package az.company.service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class SendMailServiceImpl implements SendMailService {

    private final JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String hostMail;

    @Value("classpath:/logo/logo.png")
    private Resource resourceFile;

    @Override
    public void send(String sendTo, String subject, String htmlBody) throws MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
        helper.setFrom(hostMail);
        helper.setTo(sendTo);
        helper.setSubject(subject);
        helper.setText(htmlBody, true);
        helper.addInline("logo.png", resourceFile);
        javaMailSender.send(message);
        log.info("Mail sent to {}", sendTo);
    }
}
