package az.company.service;

import az.company.dto.UserInfoDto;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.mail.MessagingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

@Service
@RequiredArgsConstructor
@Slf4j
public class PasswordResetEmailServiceImpl implements PasswordResetEmailService {

    private static final String SUBJECT = "Verification for password reset";
    private static final String RESET_PASSWORD_TEXT =
            "To reset your password, please click on this link. If you think this is by not you," +
                    " and you received the email by accident, please ignore it.";

    private final FreeMarkerConfigurer freemarkerConfigurer;
    private final SendMailService sendMailService;

    @Value("${spring.mail.username}")
    private String hostMail;

    private String url = "localhost:9090"; // TODO: 12/7/2022

    @Override
    public void sendEmail(UserInfoDto dto) throws IOException, TemplateException, MessagingException {
        Map<String, Object> templateModel = new HashMap<>();
        templateModel.put("recipientName", dto.getName());
        templateModel.put("text", RESET_PASSWORD_TEXT);
        templateModel.put("senderName", hostMail);
        templateModel.put("resetUrl",
                String.format("http://%s/#/auth/activate-account?token=%s", url, dto.getVerificationTokenDto().getToken()));
        Template template = freemarkerConfigurer.getConfiguration().getTemplate("reset-password-template.ftl");
        String htmlBody = FreeMarkerTemplateUtils.processTemplateIntoString(template, templateModel);
        sendMailService.send(dto.getEmail(), SUBJECT, htmlBody);
        log.info("Email for password reset sent to email {}", dto.getEmail());
    }
}
