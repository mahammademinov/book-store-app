package az.company;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

@EnableKafka
@SpringBootApplication
public class NotificationsMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(NotificationsMsApplication.class, args);
    }

}
