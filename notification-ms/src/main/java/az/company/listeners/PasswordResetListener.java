package az.company.listeners;

import az.company.dto.UserInfoDto;
import az.company.service.PasswordResetEmailService;
import freemarker.template.TemplateException;
import java.io.IOException;
import javax.mail.MessagingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class PasswordResetListener {

    private final PasswordResetEmailService passwordResetEmailService;

    @KafkaListener(id = "1",
            topics = "user-management-reset-password-topic",
            groupId = "notification-group-id",
            containerFactory = "kafkaJsonListenerContainerFactory")
    public void resetPasswordListener(UserInfoDto dto) throws MessagingException, IOException, TemplateException {
        log.info("Message received : {}", dto);
        passwordResetEmailService.sendEmail(dto);
    }
}
